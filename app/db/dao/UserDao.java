package db.dao;

import models.account.User;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */
public class UserDao implements DAO<User> {

    @Override
    public User add() {
        User user = Form.form(User.class).bindFromRequest().get();
        JPA.em().persist(user);
        return user;
    }

    @Override
    public User getById(Integer id) {
        User user = JPA.em().find(User.class, id);
        return user;
    }

    public User getByEmail() {
        Query q = JPA.em()
                .createQuery("select user from account as user where user.email = ?1")
                .setParameter(1, Form.form().bindFromRequest().get("header"));
        User user = (User) q.getSingleResult();
        return user;
    }

    public User getByLogin() {
        Query q = JPA.em()
                .createQuery("select user from account as user where user.login = ?1")
                .setParameter(1, Form.form().bindFromRequest().get("login"));
        User user = (User) q.getSingleResult();
        return user;
    }

    @Override
    public void update(User entity) {
        JPA.em().refresh(entity);
    }

    @Override
    public void remove(User entity) {
        JPA.em().remove(entity);
    }

    @Override
    public List<User> getAll() {
        Query q = JPA.em().createQuery("select e from account e");
        List<User> users = q.getResultList();
        return users;
    }
}