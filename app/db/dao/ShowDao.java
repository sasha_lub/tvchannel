package db.dao;

import models.mediacontent.Genre;
import models.mediacontent.Show;
import models.mediacontent.Show;
import play.data.Form;
import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */
public class ShowDao implements DAO<Show> {

    @Override
    public Show add() {
        Show show = Form.form(Show.class).bindFromRequest().get();
        JPA.em().persist(show);
        return show;
    }

    @Override
    public Show getById(Integer id) {
        Show show = JPA.em().find(Show.class, id);
        return show;
    }

    public Show getByName() {
        Query q = JPA.em()
                .createQuery("select show from show as show where show.name = ?1")
                .setParameter(1, Form.form().bindFromRequest().get("name"));
        Show show = (Show) q.getSingleResult();
        return show;
    }

    @Override
    public void update(Show entity) {
        JPA.em().refresh(entity);
    }

    @Override
    public void remove(Show entity) {
        JPA.em().remove(entity);
    }

    @Override
    public List<Show> getAll() {
        Query q = JPA.em().createQuery("select e from show e");
        List<Show> shows = q.getResultList();
        return shows;
    }
}