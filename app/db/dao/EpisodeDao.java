package db.dao;

import models.mediacontent.Episode;
import play.data.Form;
import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */
public class EpisodeDao implements DAO<Episode> {

    @Override
    public Episode add() {
        Episode episode = Form.form(Episode.class).bindFromRequest().get();
        JPA.em().persist(episode);
        return episode;
    }

    @Override
    public Episode getById(Integer id) {
        Episode episode = JPA.em().find(Episode.class, id);
        return episode;
    }

    public Episode getByName() {
        Query q = JPA.em()
                .createQuery("select episode from episode as episode where episode.name = ?1")
                .setParameter(1, Form.form().bindFromRequest().get("name"));
        Episode episode = (Episode) q.getSingleResult();
        return episode;
    }

    @Override
    public void update(Episode entity) {
        JPA.em().refresh(entity);
    }

    @Override
    public void remove(Episode entity) {
        JPA.em().remove(entity);
    }

    @Override
    public List<Episode> getAll() {
        Query q = JPA.em().createQuery("select e from episode e");
        return q.getResultList();;
    }
}