package db.dao;

import java.util.List;

/**
 * Created by sasha on 05.05.2016.
 */
public interface DAO<T> {

    T add();

    T getById(Integer id);

    void update(T entity);

    void remove(T entity);

    List<T> getAll();
}
