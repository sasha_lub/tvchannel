package db.dao;

import models.Article;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */
public class ArticleDao implements DAO<Article> {
    @Override
    public Article add() {
        Article article = Form.form(Article.class).bindFromRequest().get();
        JPA.em().persist(article);
        return article;
    }

    @Override
    public Article getById(Integer id) {
        Article article = JPA.em().find(Article.class, id);
        return article;
    }

    public Article getByHeader() {
        Query q = JPA.em()
                .createQuery("select article from article as article where article.header = ?1")
                .setParameter(1, Form.form().bindFromRequest().get("header"));
        Article article = (Article) q.getSingleResult();
        return article;
    }

    @Override
    public void update(Article entity) {
        JPA.em().refresh(entity);
    }

    @Override
    public void remove(Article entity) {
        JPA.em().remove(entity);
    }

    @Override
    public List<Article> getAll() {
        Query q = JPA.em().createQuery("select e from article e");
        List<Article> articles = q.getResultList();
        return articles;
    }
}