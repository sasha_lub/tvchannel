package db.dao;

import models.Presenter;
import play.data.Form;
import play.db.jpa.JPA;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */
public class PresenterDao implements DAO<Presenter> {
    @Override
    public Presenter add() {
        Presenter presenter = Form.form(Presenter.class).bindFromRequest().get();
        JPA.em().persist(presenter);
        return presenter;
    }

    @Override
    public Presenter getById(Integer id) {
        Presenter presenter = JPA.em().find(Presenter.class, id);
        return presenter;
    }

    @Override
    public void update(Presenter entity) {
        JPA.em().refresh(entity);
    }

    @Override
    public void remove(Presenter entity) {
        JPA.em().remove(entity);
    }

    @Override
    public List<Presenter> getAll() {
        Query q = JPA.em().createQuery("select e from presenter e");
        List<Presenter> presenters = q.getResultList();
        return presenters;
    }
}