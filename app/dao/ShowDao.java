package dao;

import app.models.mediacontent.Show;
import app.persistance.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class ShowDao extends DAO<Show> {

    public Show getByName(String name) {
        Session session = null;
        Show show = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Show.class);
            criteria.add(Restrictions.eq("name", name));
            show = (Show)criteria.uniqueResult();
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return show;
    }
}