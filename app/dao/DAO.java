package dao;

import app.persistance.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public abstract class DAO<T> {

    public void add(T entity) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public T getById(Integer id, Class<T> clazz) {
        Session session = null;
        T entity = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            entity = session.get(clazz, id);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return entity;
    }

    public void update(T entity) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public boolean remove(T entity) {
        Session session = null;
        boolean success = false;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
            success = true;
        }finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return success;
    }

    public List<T> getAll(Class<T> clazz) {
        Session session = null;
        List<T> entities = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            entities = session.createCriteria(clazz).list();
            session.getTransaction().commit();
        }finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return entities;
    }
}
