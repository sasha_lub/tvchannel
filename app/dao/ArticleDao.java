package dao;

import app.models.Article;
import app.persistance.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by sasha on 04.05.2016.
 */
public class ArticleDao extends DAO<Article> {

    public Article getByHeader(String header) {
        Session session = null;
        Article article = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Article.class);
            criteria.add(Restrictions.eq("header", header));
            article = (Article)criteria.uniqueResult();
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return article;
    }
}