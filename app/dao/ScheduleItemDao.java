package dao;

import app.models.ScheduleItem;
import app.persistance.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */
public class ScheduleItemDao extends DAO<ScheduleItem> {

    public List<ScheduleItem> getByDate(LocalDate date) {
        Session session = null;
        List<ScheduleItem> items = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(ScheduleItem.class);
            criteria.add(Restrictions.eq("date", date));
            items = criteria.list();
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return items;
    }
}