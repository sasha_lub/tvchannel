import app.models.mediacontent.AuthorShow;
import app.models.mediacontent.Episode;
import app.models.mediacontent.Movie;
import app.models.mediacontent.Series;
import app.service.show.*;
import app.models.Article;
import app.models.Presenter;
import app.models.ScheduleItem;
import app.models.account.Role;
import app.models.account.User;
import models.mediacontent.*;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import app.service.article.ArticleService;
import app.service.article.HibernateArticleService;
import app.service.presenter.HibernatePresenterService;
import app.service.presenter.PresenterService;
import app.service.schedule.Schedule;
import service.show.*;
import app.service.user.HibernateUserService;
import app.service.user.UserService;

public class DemoDB {
    static UserService userService = new HibernateUserService();
    static ArticleService articleService = new HibernateArticleService();
    static MovieService movieService = new HibernateMovieService();
    static SeriesService seriesService = new service.show.HibernateSeriesService();
    static AuthorShowService authorShowService = new HibernateAuthorShowService();
    static PresenterService presenterService = new HibernatePresenterService();
    static Schedule schedule = new Schedule();

    public static void main(String[] args) {
        init();

//        System.out.println("##########ARTICLES##########");
//
//        List<Article> articles = articleService.getAll();
//        for (Article article : articles) {
//            System.out.println(article);
//        }
//
//        System.out.println("##########USERS##########");
//
//        List<User> users = userService.getAll();
//        for (User user : users) {
//            System.out.println(user);
//        }
//
//        System.out.println("##########PRESENTERS##########");
//
//        List<Presenter> presenters = presenterService.getAll();
//        for (Presenter presenter : presenters) {
//            System.out.println(presenter);
//        }
//
//        System.out.println("##########SCHEDULE##########");
//        List<ScheduleItem> items = schedule.getAllShows();
//        for (ScheduleItem item : items) {
//            System.out.println(item);
//        }
//
//        System.out.println("##########SHOWS##########");
//
//        List<Show> shows = movieService.getAll();
//        for (Show show : shows) {
//            System.out.println(show);
//        }
    }


    private static void init() {

        articleService.add(new Article("Что там у хохлов?", "все нормальненько"));
        articleService.add(
                new Article("Новый альбом Жанны Фриске",
                        "1.Ракировка\n" + "2.Я уеду жить в Ирак\n" + "3.Кто на горе свистнул\n"
                                + "4.Press 'ok' or 'cancer'\n" + "5.Ктоооо проживает на дне океана?\n"
                                + "6.Мои тиммейты"));
        articleService.add(new Article("ШОК! #aldec",
                "Константин Маслюк, в силу своей мудрости, лишился работы (зато не руки)"));

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        Presenter doshi = new Presenter("Максим Доши", new LocalDate(1985, 7, 22),
                "Что-то на уровне Высоцкого, а может даже и выше (с)мамка");
        Presenter grey = new Presenter("Саша Грей", new LocalDate(1988, 11, 2),
                "Что-то на уровне Высоцкого, а может даже и глубже (с)батя");
        Presenter guf = new Presenter("Гуф", new LocalDate(1977, 1, 21),
                "Что-то на уровне Высоцкого, а может даже и мертвее (с)школьник");

        presenterService.add(doshi);
        presenterService.add(grey);
        presenterService.add(guf);

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        userService.add(new User("xxxAndrYh@xxx", "pass", "Андрей", "motherfucker123@mail.ru", Role.USER));
        userService.add(new User("admin", ")|(0p@", "Георгий", "georgiy.smeshnayafamilia@nure.ua", Role.ADMIN));

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        movieService.add(new Movie("Борат", "Телеведущий из Казахстана Борат отправляется в США, "
                + "чтобы сделать репортаж об этой «величайшей в мире стране». "
                + "Однако по прибытии оказалось, что главная цель его визита — "
                + "поиски Памелы Андерсон с целью жениться на ней, а вовсе не " + "съемки документального фильма…"));
        seriesService.add(new Series("Физрук",
                "Главный герой Фома всю жизнь был «правой рукой» влиятельного "
                        + "человека с полукриминальным прошлым. Когда «хозяин» "
                        + "выгнал его на пенсию, Фома решил любым способом вернуться обратно."));
        authorShowService.add(new AuthorShow("Званый ужин", "Кто из нас не мечтал пригласить своих гостей на званый ужин "
                + "и создать незабываемую атмосферу праздника, уюта и " + "непринужденного общения?! "));
        authorShowService.add(new AuthorShow("Дурнев +1",
                "Не готовы к тонкому юмору и жесткому стебу? Оглядывайтесь по сторонам и удирайте!"));
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        Movie borat = (Movie) movieService.getByName("Борат");
        AuthorShow dinner = (AuthorShow) seriesService.getByName("Званый ужин");
        AuthorShow durnev = (AuthorShow) seriesService.getByName("Дурнев +1");
        Series fizruk = (Series) seriesService.getByName("Физрук");

        Episode fizruk1 = new Episode("Подстава", "физрука подставили", 1, 1);
        Episode fizruk2 = new Episode("Разочаровние", "усачу не светит", 1, 2);
        Episode dinner1 = new Episode("Рэп от коляна",
                "А Алена, Юлькина подруга\n\t" + "Вызывала у мужчин лишь чувство испуга.\n\t"
                        + "Тощая, вертлявая, злая, рябая\n\t" + "Соблазнила-бы только Хана Мамая.\n\t"
                        + "Юлька рядом с ней-сексуальная красавица\n\t" + "Любому олигарху и без пол-литра понравится\n\t"
                        + "Жопа целлюлитная, но вполне аппетитная.\n\t" + "Грудь хоть и отвисла, но смотрится не кисло.", 1, 1);
        Episode dinner2 = new Episode("Гостепреимная Александра", "Саша развлекает троих гостей одновременно", 1, 2);
        Episode durnev1 = new Episode("Быдло в Балаклее", "Сотрудник Aldec показывает плохие результаты", 1, 1);
        Episode durnev2 = new Episode("Быдло в Краматорске", "Еще один сотрудник Aldec потерпел неудачу", 1, 2);


        seriesService.addEpisode(fizruk, fizruk1);
        seriesService.addEpisode(fizruk, fizruk2);
        seriesService.addEpisode(dinner, dinner1);
        seriesService.addEpisode(dinner, dinner2);
        seriesService.addEpisode(durnev, durnev1);
        seriesService.addEpisode(durnev, durnev2);

        authorShowService.addPresenter(dinner, grey);
        authorShowService.addPresenter(dinner, guf);
        authorShowService.addPresenter(durnev, grey);
        authorShowService.addPresenter(dinner, doshi);
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        schedule.addItem(new ScheduleItem(new LocalDate(2018, 4, 26), new LocalTime(13, 00, 00), new LocalTime(14, 50, 0),
                borat));
        schedule.addItem(new ScheduleItem(new LocalDate(2018, 4, 26), new LocalTime(19, 20, 00), new LocalTime(19, 50, 0),
                fizruk));
        schedule.addItem(new ScheduleItem(new LocalDate(2018, 4, 27), new LocalTime(19, 20, 00), new LocalTime(19, 50, 0),
                fizruk));
        schedule.addItem(new ScheduleItem(new LocalDate(2018, 4, 27), new LocalTime(19, 50, 00), new LocalTime(21, 30, 0),
                dinner));
        schedule.addItem(new ScheduleItem(new LocalDate(2018, 4, 28), new LocalTime(11, 20, 00), new LocalTime(12, 10, 0),
                durnev));
    }
}