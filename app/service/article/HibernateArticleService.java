package service.article;

import app.db.dao.ArticleDao;
import app.models.Article;

import java.util.List;

public class HibernateArticleService implements ArticleService{
    ArticleDao dao = new ArticleDao();
    @Override
    public void add(Article article) {
        dao.add(article);
    }

    @Override
    public Article getById(Integer id) {
        return dao.getById(id, Article.class);
    }

    @Override
    public boolean remove(Article article) {
        return dao.remove(article);
    }

    @Override
    public void update(Article article) {
        dao.update(article);
    }

    @Override
    public List<Article> getAll() {
        return dao.getAll(Article.class);
    }

    @Override
    public Article getByHeader(String header) {
        return dao.getByHeader(header);
    }
}
