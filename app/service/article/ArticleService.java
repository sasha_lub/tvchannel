package service.article;

import app.models.Article;

import java.util.List;

/**
 * Created by sasha on 08.05.2016.
 */
public interface ArticleService {
    void add(Article article);

    Article getById(Integer id);

    boolean remove(Article article);

    void update(Article article);

    List<Article> getAll();

    Article getByHeader(String header);
}
