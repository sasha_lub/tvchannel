package service.schedule;

import app.db.dao.ScheduleItemDao;
import app.models.ScheduleItem;
import org.joda.time.LocalDate;

import java.util.List;

public class Schedule {

    private ScheduleItemDao dao = new ScheduleItemDao();

	public List<ScheduleItem> getAllShows() {
		return dao.getAll(ScheduleItem.class);
	}

    public List<ScheduleItem> getShowsByDate(LocalDate date) {
        return dao.getByDate(date);
    }

	public void addItem(ScheduleItem item) {
        dao.add(item);
	}

	@Override
	public String toString() {
        List<ScheduleItem> items = getAllShows();
		StringBuilder sb = new StringBuilder();
		if (!items.isEmpty()){
			sb.append("##########SCHEDULE##########").append(System.lineSeparator());
			for (ScheduleItem scheduleItem : items) {
				sb.append(scheduleItem).append(System.lineSeparator());
			}
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
}