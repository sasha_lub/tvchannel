package service.user;

import app.db.dao.UserDao;
import app.models.account.User;

import java.util.List;

public class HibernateUserService implements UserService {
    UserDao dao = new UserDao();

    @Override
    public void add(User user) {
        dao.add(user);
    }

    @Override
    public User getById(Integer id) {
        return dao.getById(id, User.class);
    }

    @Override
    public boolean remove(User user) {
        return dao.remove(user);
    }

    @Override
    public void update(User user) {
        dao.update(user);
    }

    @Override
    public List<User> getAll() {
        return dao.getAll(User.class);
    }

    @Override
    public User getByEmail(String email) {
        return null;
    }

    @Override
    public User getByLogin(String login) {
        return null;
    }
}
