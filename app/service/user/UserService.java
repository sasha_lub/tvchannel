package service.user;

import app.models.account.User;

import java.util.List;

public interface UserService {
    void add(User user);

    User getById(Integer id);

    boolean remove(User user);

    void update(User user);

    List<User> getAll();

    User getByEmail(String email);

    User getByLogin(String login);
}
