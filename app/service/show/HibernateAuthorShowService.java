package service.show;

import app.db.dao.ShowDao;
import app.models.Presenter;
import app.models.mediacontent.AuthorShow;

public class HibernateAuthorShowService extends HibernateSeriesService implements AuthorShowService {
    ShowDao dao = new ShowDao();

    @Override
    public void addPresenter(AuthorShow show, Presenter presenter) {
        show.addPresenter(presenter);
        dao.update(show);
    }

}
