package service.show;

import app.models.mediacontent.Episode;
import app.models.mediacontent.EpisodicShow;

public interface SeriesService extends MovieService {

    void addEpisode(EpisodicShow series, Episode episode);

}
