package service.show;

import app.db.dao.ShowDao;
import app.models.mediacontent.Show;

import java.util.List;

public class HibernateMovieService implements MovieService {
    private ShowDao dao = new ShowDao();

    @Override
    public void add(Show show) {
        dao.add(show);
    }

    @Override
    public Show getById(Integer id) {
        return dao.getById(id, Show.class);
    }

    @Override
    public boolean remove(Show show) {
        return dao.remove(show);
    }

    @Override
    public void changeDescription(Show show, String description) {
        show.setDescription(description);
        dao.update(show);
    }

    @Override
    public void setUrl(Show show, String url) {
        show.setUrl(url);
        dao.update(show);
    }

    @Override
    public List<Show> getAll() {
        return dao.getAll(Show.class);
    }

    @Override
    public Show getByName(String name) {
        return dao.getByName(name);
    }
}