package service.show;

import app.models.mediacontent.Show;

import java.util.List;

public interface MovieService {

    void add(Show show);

    Show getById(Integer id);

    boolean remove(Show show);

    void changeDescription(Show show, String description);

    void setUrl(Show show, String url);

    List<Show> getAll();

    Show getByName(String name);
}
