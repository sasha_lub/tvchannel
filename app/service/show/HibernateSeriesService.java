package service.show;

import app.db.dao.EpisodeDao;
import app.db.dao.ShowDao;
import app.models.mediacontent.Episode;
import app.models.mediacontent.EpisodicShow;

public class HibernateSeriesService extends HibernateMovieService implements SeriesService {
    ShowDao dao = new ShowDao();

    @Override
    public void addEpisode(EpisodicShow series, Episode episode) {
        series.addEpisode(episode);
        EpisodeDao episodeDao = new EpisodeDao();
        episodeDao.add(episode);
        dao.update(series);
    }
}
