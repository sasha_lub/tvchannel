package service.show;

import app.models.Presenter;
import app.models.mediacontent.AuthorShow;

public interface AuthorShowService extends SeriesService {

    void addPresenter(AuthorShow show, Presenter presenter);
}
