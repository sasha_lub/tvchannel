package service.presenter;

import app.models.Presenter;

import java.util.List;

/**
 * Created by sasha on 08.05.2016.
 */
public interface PresenterService {

    void add(Presenter presenter);

    Presenter getById(Integer id);

    boolean remove(Presenter presenter);

    void update(Presenter presenter);

    List<Presenter> getAll();

}

