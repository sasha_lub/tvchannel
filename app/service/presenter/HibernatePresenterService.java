package service.presenter;

import app.db.dao.PresenterDao;
import app.models.Presenter;

import java.util.List;

/**
 * Created by sasha on 08.05.2016.
 */
public class HibernatePresenterService implements PresenterService {
    PresenterDao dao = new PresenterDao();

    @Override
    public void add(Presenter presenter) {
        dao.add(presenter);
    }

    @Override
    public Presenter getById(Integer id) {
        return dao.getById(id, Presenter.class);
    }

    @Override
    public boolean remove(Presenter presenter) {
        return dao.remove(presenter);
    }

    @Override
    public void update(Presenter presenter) {
        dao.update(presenter);
    }

    @Override
    public List<Presenter> getAll() {
        return dao.getAll(Presenter.class);
    }
}
