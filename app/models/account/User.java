package models.account;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "account")
@Access( AccessType.FIELD )
public class User {

    @Id
    @GeneratedValue
    private Integer id;

    @NotBlank
    @Column(unique = true, nullable = false)
    private String login;

  //  @Pattern(regexp = "[[A-ZА-ЯЁЇІЄ]+[a-zа-яёїіє]+[0-9]+]{6,}")
    @Column(nullable = false)
    private String password;

    @Pattern(regexp = "[A-ZА-ЯЁЇІa-zа-яёїіє' -`]*", flags = Pattern.Flag.CASE_INSENSITIVE)
    @Column
    private String name;

    @Email
    @NotBlank
    @Column(nullable = false, unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    public User() {
    }

    public User(String login, String password, String name, String email, Role role) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.email = email;
        this.role = role;
    }

    @Override
    public String toString() {
        return "***User***" + System.lineSeparator()
                + "login : " + login + System.lineSeparator()
                + "password : " + password + System.lineSeparator()
                + "name : " + name + System.lineSeparator()
                + "e-mail : " + email + System.lineSeparator() +
                "role : " + role + System.lineSeparator() + System.lineSeparator();
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        } else if (null == o) {
            return false;
        } else if (this.getClass() != o.getClass()) {
            return false;
        }

        if (!this.getEmail().equals(((User) o).getEmail())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash += hash * 31 + ((this.getEmail() == null) ? 0 : this.getEmail().hashCode());

        return hash;
    }

    public Integer getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}