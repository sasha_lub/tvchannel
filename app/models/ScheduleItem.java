package models;

import app.models.mediacontent.Show;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Future;

@Entity
@Table(name = "schedule_item")
public class ScheduleItem {

    @Id
    @GeneratedValue
    private Integer id;

    @Future
    @Column
    private LocalDate date;

    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "end_time")
    private LocalTime endTime;

    @Valid
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "show_id")
    private Show show;

    public ScheduleItem() {
    }

    public ScheduleItem(LocalDate date, LocalTime startTime, LocalTime endTime, Show show) {
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.show = show;
    }

    @Override
    public String toString() {
        return "***ScheduleItem***" + System.lineSeparator()
                + "date : " + date.toString("dd.MM.yyyy") + System.lineSeparator()
                + show.getName() + System.lineSeparator()
                + "time : " + startTime.toString("HH:mm:ss") + " - " + endTime.toString("HH:mm:ss")
                + System.lineSeparator();
    }

    public Integer getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }
}