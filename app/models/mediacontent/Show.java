package models.mediacontent;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "shows")
public abstract class Show {

    @Id
    @GeneratedValue
    private Integer id;

    @Pattern(regexp = "[А-ЯA-ZЁЇІЄ0-9]+[а-яa-zёїіє0-9'` \"-@!#№$%:&,?]*")
    @NotBlank
    @Column(nullable = false, unique = true)
    private String name;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "genres")
    @Enumerated(EnumType.ORDINAL)
    private List<Genre> genres = new ArrayList<Genre>();

    @Column(columnDefinition = "text")
    private String description;

    @URL
    @Column(unique = true)
    private String url;

    public Show() {
    }

    public Show(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "***Show***" + System.lineSeparator()
                + name + System.lineSeparator()
                + description + System.lineSeparator();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Genre[] getGenres() {
        return (Genre[]) genres.toArray();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        } else if (null == o) {
            return false;
        } else if (this.getClass() != o.getClass()) {
            return false;
        }

        if (!this.getName().trim().equals(((Show) o).getName().trim())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash += hash * 31 + ((this.getName() == null) ? 0 : this.getName().hashCode());

        return hash;
    }
}