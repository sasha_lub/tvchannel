package models.mediacontent;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Table(name = "episode")
public class Episode {

    @Id
    @GeneratedValue
    private Integer id;

    @NotBlank
    @Column
    private String name;

    @Column(columnDefinition="text")
    private String description;

    @Min(value = 1)
    @Column
    @ColumnDefault(value = "1")
    private int season;

    @Min(value = 0)
    @Column
    private int episodeNumber;

    public Episode() {
    }

    public Episode(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Episode(String name, String description, int season, int episodeNumber) {
        this(name, description);
        this.season = season;
        this.episodeNumber = episodeNumber;
    }
    @Override
    public String toString() {
        return "***Episode***" + System.lineSeparator() +
                "\t" + "s" + season + "e" + episodeNumber + System.lineSeparator()
                + "\t" + name + System.lineSeparator()
                + "\t" + description + System.lineSeparator();
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        } else if (null == o) {
            return false;
        } else if (this.getClass() != o.getClass()) {
            return false;
        }

        if ((!this.getName().trim().equals(((Episode) o).getName().trim()))
                && (this.getEpisodeNumber() != ((Episode) o).getEpisodeNumber())
                && (this.getSeason() != ((Episode) o).getSeason())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash += hash * 31 + ((this.getName() == null) ? 0 : this.getName().hashCode());
        hash += hash * 31 + this.getSeason();
        hash += hash * 31 + this.getEpisodeNumber();

        return hash;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }
}
