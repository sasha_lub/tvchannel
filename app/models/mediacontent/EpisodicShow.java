package models.mediacontent;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@Entity
public abstract class EpisodicShow extends Show {
    @Valid
//  @OneToMany(mappedBy = "episodic_show")
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "episodes")
    private Set<Episode> episodes = new HashSet<Episode>();

    public EpisodicShow() {
    }

    public EpisodicShow(String name, String description) {
        super(name, description);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        if (!episodes.isEmpty()) {
            sb.append("Episodes:").append(System.lineSeparator());
            for (Episode episode : episodes) {
                sb.append("\t").append(episode);
            }
        }
        return sb.toString();
    }

    public void addEpisode(Episode episode) {
        episodes.add(episode);
    }

    public Episode[] getEpisodes() {
        return (Episode[]) episodes.toArray();
    }
}
