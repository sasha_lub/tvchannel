package models;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "article")
@Access( AccessType.FIELD )
public class Article {

    @Id
    @GeneratedValue
    private Integer id;

    @Pattern(regexp = "[А-ЯA-ZЁЇІЄа-яa-zёїіє0-9'`\"-—«»@!#№$% :&,?]+")
    @NotBlank
    @Column(nullable = false)
    private String header;

    @NotBlank
    @Column(nullable = false, columnDefinition="TEXT")
    private String text;

    public Article() {
    }

    public Article(String header, String text) {
        this.header = header;
        this.text = text;
    }

    @Override
    public String toString() {
        return "***Article***" + System.lineSeparator()
                + header + System.lineSeparator()
                + text + System.lineSeparator() + System.lineSeparator();
    }

    public Integer getId() {
        return id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}